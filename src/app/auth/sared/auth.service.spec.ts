import { TestBed, inject } from '@angular/core/testing';

import { AuthService } from './auth.service';
import {TestsModule} from '../../shared/tests.modules';
import {Injector} from '@angular/core';

describe('AuthService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [TestsModule],
      providers: [AuthService]
    });
  });

  it('should be created', inject([AuthService], (service: AuthService) => {
    expect(service).toBeTruthy();
  }));

  it('should be log a user', inject([AuthService], (service) => {
    service.login({email: 'peter@klaven', password: 'cityslicka'}).subscribe((user) => {
      expect(user.token).toBe('QpwL5tke4Pnpja7X');
    });
  }));

  it('should be return error login', inject([AuthService], (service) => {
    service.login({email: 'peter@klaven'}).subscribe((user) => {
      expect(user.error).toBe('Missing password');
    });
  }));


});
