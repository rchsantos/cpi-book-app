import { Injectable } from '@angular/core';
import {HttpClient, HttpErrorResponse, HttpHeaders} from '@angular/common/http';
import {BehaviorSubject} from 'rxjs/BehaviorSubject';
import {Data, Router} from '@angular/router';
import {Users} from './users.model';
import {environment} from '../../../environments/environment';
import { catchError, map, filter } from 'rxjs/operators';
import {Observable} from 'rxjs/Observable';

@Injectable()
export class AuthService {
  private headers: HttpHeaders;

  userProfile: any;
  // Create a stream of logged in status to communicate throughout app
  loggedIn: boolean;
  loggedIn$ = new BehaviorSubject<boolean>(this.loggedIn);
  user: BehaviorSubject<Users> = new BehaviorSubject(null);

  constructor(
    private httpService: HttpClient,
    private router: Router
  ) {
    this.headers = new HttpHeaders({'Content-Type': 'application/json'});

    // If authenticated, set local profile property and update login status subject
    // If token is expired, log out to clear any data from localStorage
    if (this.authenticated) {
      this.userProfile = JSON.parse(localStorage.getItem('profile'));
      this.setLoggedIn(true);
    } else {
      this.logout();
    }
  }

  /**
   * Update login status subject
   * @param {boolean} value
   */
  setLoggedIn(value: boolean) {
    this.loggedIn$.next(value);
    this.loggedIn = value;
  }

  /**
   * Method to log user, set tokens and profile and update login status subject
   * @param {Users} user
   * @returns {Observable<any>}
   */
  login(user: Users) {
    const credentials = { email: user.email, password: user.password };

    return this.httpService
      .post(`${environment.apiHost}/login`, JSON.stringify(credentials), { headers: this.headers } )
      .pipe(
        map(res => {
          console.log('Login', res);

          // set tokens
          if (res['token']) {
            // get exactly date to set expire session
            const expirityDate = new Date().getDate();

            // set session
            this.setSession({expiresIn: expirityDate, accessToken: res['token']}, credentials.email);

            // get user name
            this.user.next(AuthService.extractname(credentials.email));

            // redirect user to home page after user logged
            this.router.navigate(['']);
          }

        }),
        catchError(error => this.handleError(error))
      );
  }

  /**
   * Remove tokens and profile and update login status subject
   */
  logout() {
    localStorage.removeItem('token');
    localStorage.removeItem('profile');
    localStorage.removeItem('expires_at');
    this.userProfile = undefined;
    this.setLoggedIn(false);
  }

  /**
   * Build session objects
   * @param authResult
   * @param {string} profile
   */
  private setSession(authResult: any, profile: string) {
    const expTime = authResult.expiresIn * 1000 + Date.now();
    // Save session data and update login status subject
    localStorage.setItem('token', authResult.accessToken);
    localStorage.setItem('profile', JSON.stringify(profile));
    localStorage.setItem('expires_at', JSON.stringify(expTime));
    this.userProfile = profile;
    this.setLoggedIn(true);
  }

  /**
   * Return if user is authenticated
   * @returns {boolean}
   */
  get authenticated(): boolean {
    // Check if current date is greater than expiration
    const expiresAt = JSON.parse(localStorage.getItem('expires_at'));
    return Date.now() < expiresAt;
  }

  /**
   * Check if the user is authenticated
   */
  public isAuthenticated() {
    // accessToken may be expired, we only test the presence of the token
    return this.authenticated;
  }

  /**
   * Handle Http operation that failed
   * @param error
   * @returns {ErrorObservable}
   */
  private handleError(error: any) {
    if (error instanceof Response) {
      return Observable.throw(error.json()['error'] || 'Reqres API error');
    }

    console.error(
      `Backend returned code ${error.status}, ` +
      `body was: ${error}`);

    return Observable.throw(error || 'Reqres API error');
  }

  /**
   * Extract name of User by email
   */
  static extractname(name) {
    return name.split('@').pop();
  }

}
