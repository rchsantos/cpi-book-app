import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators, } from '@angular/forms';
import {AuthService} from '../sared/auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  hide = true;
  loginForm: FormGroup;

  constructor(
    private fb: FormBuilder,
    private authService: AuthService
  ) {
    // this should be declared at runtime
    this.loginForm = this.fb.group({
      'email' : ['', Validators.required],
      'password' : ['', Validators.required]
    });
  }

  ngOnInit() {
  }

  // Submit login form
  onSubmitLogin(post) {
    this.authService.login({
      email: post.email,
      password: post.password
    }).subscribe(res => console.log('Logged', res));
  }

  // get errors messages in form submission
  getErrorMessage() {
    return this.loginForm.hasError('required') ? 'You must enter a value' :
      this.loginForm.hasError('email') ? 'Not a valid email' : '';
  }

}
