// deep
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

// services
import { BooksService } from './shared/books.service';
import { BooksListComponent } from './books-list/books-list.component';
import { MaterialModule } from '../shared/material.module';

@NgModule({
  imports: [
    CommonModule,
    MaterialModule
  ],
  exports: [
    BooksListComponent
  ],
  declarations: [ BooksListComponent ],
  providers: [ BooksService ]
})
export class BooksModule { }
