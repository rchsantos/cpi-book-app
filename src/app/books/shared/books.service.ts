import { Injectable } from '@angular/core';
import {HttpClient, HttpErrorResponse, HttpHeaders} from '@angular/common/http';
import { environment } from '../../../environments/environment';

// Observable
import { Observable } from 'rxjs/Observable';
import { catchError, map } from 'rxjs/operators';
import { of } from 'rxjs/observable/of';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';

// Books imports
import { Book } from './books.model';

@Injectable()
export class BooksService {
  private headers: HttpHeaders;
  public books: BehaviorSubject<Book[]> = new BehaviorSubject(null); // stock books library

  constructor(private httpService: HttpClient) {
    this.headers = new HttpHeaders({'Content-Type': 'application/json'});
  }

  /**
   *  GET all Books from the API
   * @param {string} page
   * @param {string} ipp
   * @returns {Observable<any>}
   */
  getAllBooks(page: string = '1', ipp: string = '3') {
    return this.httpService
      .get(`${environment.apiHost}/books?page=${page}&per_page=${ipp}`)
      .pipe(
        map(books => {
          this.books.next(Object.assign([], books));
          return this.books.getValue();
        }),
        catchError(error => this.handleError(error))
      );
  }

  /**
   * GET Book whose name contains search term
   * @param terms
   * @returns {any}
   */
  searchBooksByName(terms: string) {
    if (!terms) {
      // if not search term, return empty book array.
      return of([]);
    }

     return this.books
       .pipe(
         map(books => books['data'].filter((book: Book) => book.name === terms))
       );
  }

  /**
   * Handle Http operation that failed
   * @param error
   * @returns {ErrorObservable}
   */
  private handleError(error: any) {
    if (error instanceof Response) {
      return Observable.throw(error.json()['error'] || 'Reqres API error');
    }
    console.error(
      `Backend returned code ${error.status}, ` +
      `body was: ${error.error}`);

    return Observable.throw(error || 'Reqres API error');
  }

}
