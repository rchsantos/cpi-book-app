import { TestBed, inject } from '@angular/core/testing';

import { BooksService } from './books.service';
import {TestsModule} from '../../shared/tests.modules';

describe('BooksService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [TestsModule],
      providers: [BooksService]
    });
  });

  it('should be created', inject([BooksService], (service: BooksService) => {
    expect(service).toBeTruthy();
  }));

  it('Should return an Observable<Array<Book>>',
    inject([BooksService], (booksService) => {
      booksService.getAllBooks().subscribe((books) => {
        expect(books.data.length).toBe(3);
        expect(books.data[0].id).toBe(1);
        expect(books.data[1].id).toBe(2);
        expect(books.data[2].id).toBe(3);
      });
    })
  );

  it('Should return search by name',
    inject([BooksService], (bookService) => {
      bookService.getAllBooks().subscribe(() => {
        bookService.searchBooksByName('cerulean').subscribe((book) => {
          expect(book.name).toBe('cerulean');
        });
      });
    }));

});
