import {Component, OnInit} from '@angular/core';
import {BooksService} from '../shared/books.service';

import {Book} from '../shared/books.model';
import {Subject} from 'rxjs/Subject';
import {
  debounceTime, distinctUntilChanged, switchMap
} from 'rxjs/operators';


@Component({
  selector: 'app-books-list',
  templateUrl: './books-list.component.html',
  styleUrls: ['./books-list.component.scss']
})
export class BooksListComponent implements OnInit {

  // variables
  books: Book[];
  page: string;
  totalItems: string;
  totalPages: string;
  perPage: string;

  searchTerms = new Subject<string>();

  constructor(private booksServices: BooksService) {
  }

  ngOnInit() {

    // get all books when component started
    this.booksServices.getAllBooks('1', '12')
      .subscribe(books => {
        this.books = books.data;
        this.page = books.page;
        this.totalItems = books.total;
        this.totalPages = books.total_pages;
        this.perPage = books.per_page;
      });

    // get book by name
    this.searchTerms.pipe(
      debounceTime(300),
      distinctUntilChanged(),
      switchMap((term: string) => this.booksServices.searchBooksByName(term))
    )
      .subscribe(res => {
        if (res && res.length) {
          console.log('nama', res);
          this.books = Object.assign([], res);
        } else {
          // if call return null display all list
          this.booksServices.books
            .subscribe(books => {
              this.books = Object.assign([], books['data']);
            });
        }
      });
  }

  // Push a search term into the observable stream
  search(term: string) {
    this.searchTerms.next(term);
  }

}
