import {Component, OnInit} from '@angular/core';
import {AuthService} from './auth/sared/auth.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  userName$; // user to display in header

  constructor(
    private authService: AuthService,
    private router: Router
  ) { }

  ngOnInit() {
    this.userName$ = this.authService.user;
  }

  // clear authenticate user
  onLogout() {
    this.authService.logout();
    this.router.navigate(['/login']);
  }

}
