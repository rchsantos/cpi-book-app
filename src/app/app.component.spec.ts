import { TestBed, async } from '@angular/core/testing';

import { AppComponent } from './app.component';
import {TestsModule} from './shared/tests.modules';
import {AuthService} from './auth/sared/auth.service';

describe('AppComponent', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        TestsModule
      ],
      providers: [AuthService],
      declarations: [
        AppComponent
      ],
    }).compileComponents();
  }));

  it('should create the app', async(() => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
  }));

});
