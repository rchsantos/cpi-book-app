import {RouterModule, Routes} from '@angular/router';
import {ModuleWithProviders} from '@angular/compiler/src/core';
import {LoginComponent} from './auth/login/login.component';
import {BooksListComponent} from './books/books-list/books-list.component';
import {AuthenticatedGuard} from './guards/authenticated.guard';

const appRoutes: Routes = [
  {
    path: 'login',
    component: LoginComponent
  },
  {
    path: '',
    component: BooksListComponent,
    canActivate: [ AuthenticatedGuard ],
  }
];

export const routing: ModuleWithProviders = RouterModule.forRoot(appRoutes);
