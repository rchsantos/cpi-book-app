import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import {HTTP_INTERCEPTORS, HttpClientModule} from '@angular/common/http';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';

import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MaterialModule } from './shared/material.module';
import { BooksModule } from './books/books.module';
import { BooksService } from './books/shared/books.service';
import {AuthService} from './auth/sared/auth.service';
import {AuthModule} from './auth/auth.module';
import {routing} from './app.routes';
import {AuthenticatedGuard} from './guards/authenticated.guard';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    HttpClientModule,
    BrowserModule,
    BrowserAnimationsModule,
    MaterialModule,
    FormsModule,
    BooksModule,
    AuthModule,
    ReactiveFormsModule,
    routing
  ],
  providers: [
    BooksService,
    AuthService,
    AuthenticatedGuard
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
