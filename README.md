# CpiBookApp

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 1.3.2.

## Usage

`npm i` - Installs everything needed

`npm start` - Starts the app. Then, go to `localhost:4200`

`npm run test` - Runs unit tests with karma and jasmine

`npm run e2e` - Runs end to end tests

`npm run build` - Builds the app for production

`npm run lint` - Runs the linter (tslint)

## License

MIT

Enjoy :metal:

We are always happy to hear your feedback!

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).
